<?php

namespace Tests\Unit\Repositories;


use App\Models\Club;
use App\Repositories\ClubRepository;
use App\Repositories\GameRepository;
use App\Repositories\SeasonRepository;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class ClubRepositoryTest extends TestCase
{
    use DatabaseMigrations;


    protected function setUp(): void
    {
        parent::setUp();
        $this->artisan('db:seed');
        $this->seasonRepository = new SeasonRepository();
        $this->gameRepository = new GameRepository($this->seasonRepository);
        $this->repository = new ClubRepository($this->gameRepository);
    }
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_get_clubs()
    {
        $clubs = $this->repository->getClubs();
        $this->assertIsObject($clubs);
        $this->assertCount(4, $clubs);
        foreach ($clubs as $club){
            $this->assertInstanceOf(Club::class, $club);
        }
    }
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_get_clubs_ids()
    {
        $ids = $this->repository->getClubsIds();
        $this->assertIsArray($ids);
        $this->assertCount(4, $ids);
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_get_clubs_count()
    {
        $count = $this->repository->getClubsCount();
        $this->assertIsInt($count);
        $this->assertTrue(4 === $count);
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_get_games()
    {
        $club = Club::first();
        $this->assertInstanceOf(Club::class, $club);
        $games = $this->repository->getGames($club);
        $this->assertIsObject($games);
        $this->assertIsInt($games->count());
        $this->assertTrue(0 === $games->count());
    }
}
