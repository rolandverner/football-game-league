<?php

namespace Tests\Unit\Repositories;

use App\Models\Season;
use App\Repositories\SeasonRepository;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class SeasonRepositoryTest extends TestCase
{
    use DatabaseMigrations;

    private const SEASON_COUNT = 4;

    protected function setUp(): void
    {
        parent::setUp();
        $this->artisan('db:seed');
        $this->seasons = Season::factory(self::SEASON_COUNT)->create();
        $this->repository = new SeasonRepository();
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_get_all_seasons()
    {
        $seasons = $this->repository->getAllSeasons();
        $this->assertCount(1, $seasons);

        $this->assertIsObject($seasons);

        foreach ($seasons as $season) {
            $this->assertInstanceOf(Season::class, $season);
        }
    }

    /**
     * @return void
     */
    public function test_get_latest_season()
    {
        $season = $this->repository->getLatestSeason();
        $this->assertIsObject($season);
        $this->assertArrayHasKey('name', $season);
    }

    /**
     * @return void
     */
    public function test_create_season()
    {
        $season = $this->repository->createSeason(['name' => 1]);
        $this->assertIsObject($season);
        $this->assertArrayHasKey('name', $season);
        $this->assertTrue($season->name === 1);
    }




}
