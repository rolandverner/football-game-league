<?php

namespace App\Http\Livewire;

use App\Contracts\Game\GameService as GameServiceContract;
use App\Models\Season;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class Play extends Component
{
    public Season $season;

    private GameServiceContract $gameService;

    /**
     * @param GameServiceContract $gameService
     * @return void
     */
    public function boot(GameServiceContract $gameService)
    {
        $this->gameService = $gameService;
    }

    /**
     * @return void
     */
    public function playOneGame()
    {
        $this->gameService->play($this->season, true);
    }

    /**
     * @return Factory|View|Application
     */
    public function render(): Factory|View|Application
    {
        return view('livewire.play');
    }
}
