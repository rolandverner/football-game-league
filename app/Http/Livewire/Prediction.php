<?php

namespace App\Http\Livewire;

use App\Contracts\Prediction\PredictionService as PredictionServiceContract;
use App\Models\Season;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class Prediction extends Component
{
    public int $week;
    public Season $season;

    private PredictionServiceContract $predictionService;

    /**
     * @param PredictionServiceContract $predictionService
     * @return void
     */
    public function boot(PredictionServiceContract $predictionService)
    {
        $this->predictionService = $predictionService;
    }

    /**
     * @return Factory|View|Application
     */
    public function render(): Factory|View|Application
    {
        return view('livewire.prediction', [
            'predictions' => $this->predictionService->getPrediction($this->season, $this->week),
        ]);
    }
}
