<?php

namespace App\Http\Livewire;

use App\Contracts\Game\GameService as GameServiceContract;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class StartGame extends Component
{

    private GameServiceContract $gameService;

    /**
     * @param GameServiceContract $gameService
     * @return void
     */
    public function boot(GameServiceContract $gameService)
    {
        $this->gameService = $gameService;
    }

    /**
     * @return void
     */
    public function startGame()
    {
        $this->gameService->genGame();
        $this->emit('reRenderSeason');
    }

    /**
     * @return Application|Factory|View
     */
    public function render(): View|Factory|Application
    {
        return view('livewire.start-game');
    }
}
