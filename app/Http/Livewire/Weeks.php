<?php

namespace App\Http\Livewire;

use App\Contracts\Club\ClubRepositoryInterface;
use App\Contracts\Game\GameRepositoryInterface;
use App\Models\Season;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class Weeks extends Component
{
    /**
     * @var
     */
    public $season;
    /**
     * @var
     */
    public $week;

    private ClubRepositoryInterface $clubRepository;
    private GameRepositoryInterface $gameRepository;

    /**
     * @param ClubRepositoryInterface $clubRepository
     * @param GameRepositoryInterface $gameRepository
     * @return void
     */
    public function boot(ClubRepositoryInterface $clubRepository, GameRepositoryInterface $gameRepository)
    {
        $this->clubRepository = $clubRepository;
        $this->gameRepository = $gameRepository;
    }

    /**
     * @var string[]
     */
    protected $listeners = [
        'reRenderWeeks'
    ];

    /**
     * @param Season $season
     * @param $week
     * @return void
     */
    public function mount(Season $season, $week){
        $this->season = $season;
        $this->week = $week;
    }

    /**
     * @return void
     */
    public function reRenderWeeks()
    {
        $this->render();
    }

    /**
     * @return Application|Factory|View
     */
    public function render(): View|Factory|Application
    {
        $weeks = $this->gameRepository->getAllPlayedWeeks($this->season, $this->week);
        $clubs = $this->clubRepository->getClubs();
        return view('livewire.weeks',[
            'games' => $weeks,
            'clubs' => $clubs,
            'clubRepository' => $this->clubRepository,
            'gameRepository' => $this->gameRepository,
        ]);
    }
}
