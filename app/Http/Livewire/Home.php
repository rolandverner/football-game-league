<?php

namespace App\Http\Livewire;

use App\Contracts\Season\SeasonRepositoryInterface;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;
use Livewire\WithPagination;

class Home extends Component
{
    use WithPagination;

    private SeasonRepositoryInterface $seasonRepository;

    /**
     * @param SeasonRepositoryInterface $seasonRepository
     * @return void
     */
    public function boot(SeasonRepositoryInterface $seasonRepository)
    {
        $this->seasonRepository = $seasonRepository;
    }

    /**
     * @var string
     */
    protected string $paginationTheme = 'tailwind';

    /**
     * @var string[]
     */
    protected $listeners = [
        'reRenderSeason'
    ];

    /**
     * @return void
     */
    public function reRenderSeason()
    {
        $this->render();
    }

    /**
     * @return Application|Factory|View
     */
    public function render(): View|Factory|Application
    {
        $seasons = $this->seasonRepository->getAllSeasonsSorted();
        return view('livewire.home', [
            'seasons' => $seasons,
        ]);
    }
}
