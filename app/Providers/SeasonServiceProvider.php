<?php

namespace App\Providers;

use App\Contracts\Season\SeasonContract;
use App\Contracts\Season\SeasonService as SeasonServiceContract;
use App\Services\SeasonService;
use App\Models\Season;
use Illuminate\Support\ServiceProvider;

class SeasonServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerContracts();
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register Seasons classes in the container.
     *
     * @return void
     */
    protected function registerContracts(): void
    {
        $this->app->bind(SeasonContract::class, Season::class);
        $this->app->singleton(SeasonServiceContract::class, SeasonService::class);
    }
}
