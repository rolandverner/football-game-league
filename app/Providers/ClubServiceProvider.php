<?php

namespace App\Providers;



use App\Contracts\Club\ClubContract;
use App\Contracts\Club\ClubService as ClubServiceContract;
use App\Models\Club;
use App\Services\ClubService;
use Illuminate\Support\ServiceProvider;

class ClubServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerContracts();
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register Seasons classes in the container.
     *
     * @return void
     */
    protected function registerContracts(): void
    {
        $this->app->bind(ClubContract::class, Club::class);
        $this->app->singleton(ClubServiceContract::class, ClubService::class);
    }
}
