<?php

namespace App\Contracts\Club;

use App\Models\Club;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;

interface ClubRepositoryInterface
{
    /**
     * Club entity.
     *
     * @param bool $paginate
     * @return Collection|LengthAwarePaginator|array
     */
    public function getClubs( bool $paginate): Collection|LengthAwarePaginator|array;

    /**
     * @return array
     */
    public function getClubsIds(): array;

    /**
     * @return int
     */
    public function getClubsCount(): int;


    /**
     * @param $club
     * @param $season
     * @param $week
     * @return Collection
     */
    public function getGames($club, $season, $week): Collection;

    /**
     * @param Club $club
     * @param $season
     * @param $week
     * @return float|int
     */
    public function getPoints(Club $club, $season, $week): float|int;

    /**
     * @param Club $club
     * @param $season
     * @param $week
     * @return Collection
     */
    public function getDraws(Club $club, $season, $week): Collection;

    /**
     * @param Club $club
     * @param $season
     * @param $week
     * @return Collection
     */
    public function getWins(Club $club, $season, $week): Collection;

    /**
     * @param Club $club
     * @param $season
     * @param $week
     * @return Collection
     */
    public function getLost(Club $club, $season, $week): Collection;

    /**
     * @param Club $club
     * @param $season
     * @param $week
     * @return float|int
     */
    public function getGoalDiff(Club $club, $season, $week): float|int;

    /**
     * @param Club $club
     * @param $season
     * @param $week
     * @return float|int
     */
    public function goalsScored(Club $club, $season, $week): float|int;

    /**
     * @param Club $club
     * @param $season
     * @param $week
     * @return float|int
     */
    public function goalsReceived(Club $club, $season, $week): float|int;

}
