<?php

namespace App\Repositories;

use App\Contracts\Club\ClubRepositoryInterface;
use App\Contracts\Game\GameRepositoryInterface;
use App\Models\Club;
use App\Models\Game;
use App\Models\Season;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;

class ClubRepository implements ClubRepositoryInterface
{

    private GameRepositoryInterface $gameRepository;

    /**
     * @param GameRepositoryInterface $gameRepository
     */
    public function __construct(
        GameRepositoryInterface $gameRepository,
    ) {
        $this->gameRepository = $gameRepository;
    }

    /**
     * @param $paginate
     * @return Collection|LengthAwarePaginator|array
     */
    public function getClubs($paginate = false
    ): Collection|LengthAwarePaginator|array {
        $clubs = Club::query();
        if ($paginate) {
            return $clubs->paginate();
        }
        return $clubs->get();
    }

    /**
     * @return array
     */
    public function getClubsIds(): array
    {
        return Club::pluck('id')->toArray();
    }

    /**
     * @return int
     */
    public function getClubsCount(): int
    {
        return Club::count();
    }


    /**
     * @param $club
     * @param $season
     * @param $week
     * @return Collection
     */
    public function getGames($club, $season = null, $week = null): Collection
    {
        $first_game = $club->firstGame();
        $second_game = $club->secondGame();
        if (isset($season)) {
            $first_game->seasonId($season->id);
            $second_game->seasonId($season->id);
        }
        if (isset($week)) {
            $first_game->week($week, '<=');
            $second_game->week($week, '<=');
        }
        $first_game = $first_game->get();
        $second_game = $second_game->get();
        return $first_game->merge($second_game);
    }

    /**
     * @param Club $club
     * @param null $season
     * @param null $week
     * @return float|int
     */
    public function getPoints(Club $club, $season = null, $week = null): float|int
    {
        return ($this->getWins($club, $season, $week)->count() * Club::WIN_POINT) + $this->getDraws(
                $club,
                $season,
                $week
            )->count();
    }

    /**
     * @param Club $club
     * @param null $season
     * @param null $week
     * @return Collection
     */
    public function getDraws(Club $club, $season = null, $week = null): Collection
    {
        return $this->getGames($club)->filter(
            fn(Game $game) => is_null(
                    $this->gameRepository->getWinner($game)
                ) && (!$season instanceof Season || $game->season_id === $season->id) && (!$week || $game->week <= $week)
        );
    }

    /**
     * @param Club $club
     * @param null $season
     * @param null $week
     * @return Collection
     */
    public function getWins(Club $club, $season = null, $week = null): Collection
    {
        return $this->getGames($club)->filter(
            fn(Game $game) => $this->gameRepository->getWinner(
                    $game
                ) instanceof Club && $this->gameRepository->getWinner(
                    $game
                )->id === $club->id && (!$season instanceof Season || $game->season_id === $season->id) && (!$week || $game->week <= $week)
        );
    }

    /**
     * @param Club $club
     * @param null $season
     * @param null $week
     * @return Collection
     */
    public function getLost(Club $club, $season = null, $week = null): Collection
    {
        return $this->getGames($club)->filter(
            fn(Game $game) => $this->gameRepository->getWinner(
                    $game
                ) instanceof Club && $this->gameRepository->getWinner(
                    $game
                )->id != $club->id && (!$season instanceof Season || $game->season_id === $season->id) && (!$week || $game->week <= $week)
        );
    }

    /**
     * @param Club $club
     * @param null $season
     * @param null $week
     * @return float|int
     */
    public function getGoalDiff(Club $club, $season = null, $week = null): float|int
    {
        return abs($this->goalsScored($club, $season, $week) - $this->goalsReceived($club, $season, $week));
    }

    /**
     * @param Club $club
     * @param null $season
     * @param null $week
     * @return float|int
     */
    public function goalsScored(Club $club, $season = null, $week = null): float|int
    {
        $goals = [
            $this->getGames($club, $season, $week)->map(
                fn(Game $game
                ) => $game->club_first_id === $club->id ? $game->club_first_goals : $game->club_second_goals
            )->toArray()
        ];
        return array_sum($goals[0]);
    }

    /**
     * @param Club $club
     * @param null $season
     * @param null $week
     * @return float|int
     */
    public function goalsReceived(Club $club, $season = null, $week = null): float|int
    {
        $goals = [
            $this->getGames($club, $season, $week)->map(
                fn(Game $game
                ) => $game->club_first_id === $club->id ? $game->club_second_goals : $game->club_first_goals
            )->toArray()
        ];
        return array_sum($goals[0]);
    }

}
