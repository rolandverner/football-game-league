<?php

namespace App\Repositories;

use App\Contracts\Season\SeasonRepositoryInterface;
use App\Models\Season;
use Illuminate\Contracts\Pagination\LengthAwarePaginator as LengthAwarePaginatorAlias;
use Illuminate\Database\Eloquent\Model;

class SeasonRepository implements SeasonRepositoryInterface
{

    /**
     * @return LengthAwarePaginatorAlias
     */
    public function getAllSeasons(): LengthAwarePaginatorAlias
    {
        return Season::paginate(1);
    }

    /**
     * @return LengthAwarePaginatorAlias
     */
    public function getAllSeasonsSorted(): LengthAwarePaginatorAlias
    {
        return Season::orderByDesc('id')->paginate(1);
    }


    /**
     * @return Season|Model|null
     */
    public function getLatestSeason(): Season|Model|null
    {
        return Season::latest()->first();
    }

    /**
     * @param array $seasonParams
     * @return Season|Model
     */
    public function createSeason(array $seasonParams): Season|Model
    {
        return Season::create($seasonParams);
    }

    /**
     * @param Season $season
     * @param Season $previousSeason
     * @return int|false
     */
    public function incrementSeasonName(Season $season, Season $previousSeason): bool|int
    {
        return $season->increment('name', $previousSeason->name);
    }


    /**
     * @param Season $season
     * @return array
     */
    public function getWeeks(Season $season): array
    {
        return $season->games->unique('week')->pluck('week')->toArray();
    }

    /**
     * @param Season $season
     * @return mixed
     */
    public function getPlayedWeeks(Season $season): mixed
    {
        return $season->games()->played()->get()->unique('week')->pluck('week')->toArray();
    }

    /**
     * @param Season $season
     * @return array
     */
    public function getLeftWeeks(Season $season): array
    {
        return array_values(array_diff($this->getWeeks($season), $this->getPlayedWeeks($season)));
    }

    /**
     * @param Season $season
     * @return false|mixed
     */
    public function getNextWeek(Season $season): mixed
    {
        $array = array_diff($this->getWeeks($season), $this->getPlayedWeeks($season));
        return reset($array);
    }

    /**
     * @param Season $season
     * @return bool
     */
    public function isFinished(Season $season): bool
    {
        return count($this->getPlayedWeeks($season)) === 4;
    }
}
