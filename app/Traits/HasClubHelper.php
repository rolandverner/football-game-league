<?php

namespace App\Traits;

use App\Providers\ClubServiceProvider;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;


trait HasClubHelper
{

    /**
     * @param bool $paginate
     * @return Collection|LengthAwarePaginator|array
     */
    public function getClubs(bool $paginate = false): Collection|LengthAwarePaginator|array
    {
        return app(ClubServiceProvider::class)->getClubs($paginate);
    }

    /**
     * @return array
     */
    public function getClubsIds(): array
    {
        return app(ClubServiceProvider::class)->getClubsIds();
    }

    /**
     * @return int
     */
    public function getClubsCount(): int
    {
        return app(ClubServiceProvider::class)->getClubsCount();
    }

}
