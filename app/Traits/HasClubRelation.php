<?php

namespace App\Traits;

use App\Contracts\Club\ClubContract;
use Illuminate\Database\Eloquent\Relations\BelongsTo;


trait HasClubRelation
{
    /**
     * @return BelongsTo
     */
    public function club_first(): BelongsTo
    {
        return $this->belongsTo(app(ClubContract::class), 'club_first_id');
    }

    /**
     * @return BelongsTo
     */
    public function club_second(): BelongsTo
    {
        return $this->belongsTo(app(ClubContract::class), 'club_second_id');
    }

}
