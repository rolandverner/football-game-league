<?php

namespace App\Models;

use App\Contracts\Club\Clubbable as ClubbableContract;
use App\Contracts\Game\GameContract;
use App\Contracts\Season\Seasonable as SeasonableContract;
use App\Traits\Clubbable;
use App\Traits\Seasonable;
use Database\Factories\GameFactory;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Game
 *
 * @property-read Club|null $club_first
 * @property-read Club|null $club_second
 * @method static GameFactory factory(...$parameters)
 * @method static Builder|Game newModelQuery()
 * @method static Builder|Game newQuery()
 * @method static Builder|Game played(bool $type = true)
 * @method static Builder|Game query()
 * @method static Builder|Game seasonId($type)
 * @method static Builder|Game week(string $type, string $operator = '=')
 * @mixin Eloquent
 */
class Game extends Model implements GameContract, SeasonableContract, ClubbableContract
{
    use HasFactory;
    use Seasonable;
    use Clubbable;

    public const WEEK = [
        1 => [[1, 4], [2, 3]],
        2 => [[2, 3], [4, 1]],
        3 => [[3, 2], [1, 4]],
        4 => [[4, 1], [3, 2]]
    ];

    /**
     * @var string[]
     */
    protected $fillable = [
        'season_id',
        'week',
        'club_first_id',
        'club_second_id',
        'club_first_goals',
        'club_second_goals',
        'played'
    ];


    /**
     * @param $query
     * @param bool $type
     * @return mixed
     */
    public function scopePlayed($query, bool $type = true): mixed
    {
        return $query->where('played', $type);
    }

    /**
     * @param $query
     * @param $type
     * @return mixed
     */
    public function scopeSeasonId($query, $type): mixed
    {
        return $query->where('season_id', $type);
    }

    /**
     * @param $query
     * @param $type
     * @param string $operator
     * @return mixed
     */
    public function scopeWeek($query, $type, string $operator = '='): mixed
    {
        return $query->where('week', $operator, $type);
    }

}
