<div>
    <div class="container mx-auto sm:px-6 lg:px-8">
        @foreach($games as $game)
            <div class="mt-8 flex">
                <div class="flex grid grid-cols-1 md:grid-cols-2 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg m-4 w-2/3">
                    <div class="p-6">
                        <h2 class="px-3 py-3 text-left text-xs font-medium text-gray-900 dark:text-white uppercase tracking-wider text-center">League Table</h2>
                        <table class="min-w-full divide-y divide-gray-200 dark:divide-gray-800 table-auto">
                            <thead class="bg-gray-50 dark:bg-gray-900">
                            <tr>
                                <th scope="col" class="px-3 py-3 text-xs font-medium text-gray-900 dark:text-white uppercase tracking-wider text-left">
                                    Teams
                                </th>
                                <th scope="col" class="px-3 py-3 text-xs font-medium text-gray-900 dark:text-white uppercase tracking-wider text-center">
                                    PTS
                                </th>
                                <th scope="col" class="px-3 py-3 text-xs font-medium text-gray-900 dark:text-white uppercase tracking-wider text-center">
                                    P
                                </th>
                                <th scope="col" class="px-3 py-3 text-xs font-medium text-gray-900 dark:text-white uppercase tracking-wider text-center">
                                    W
                                </th>
                                <th scope="col" class="px-3 py-3 text-xs font-medium text-gray-900 dark:text-white uppercase tracking-wider text-center">
                                    D
                                </th>
                                <th scope="col" class="px-3 py-3 text-xs font-medium text-gray-900 dark:text-white uppercase tracking-wider text-center">
                                    L
                                </th>
                                <th scope="col" class="px-3 py-3 text-xs font-medium text-gray-900 dark:text-white uppercase tracking-wider text-center">
                                    GD
                                </th>
                            </tr>
                            </thead>
                            <tbody class="divide-gray-200 dark:divide-gray-900">
                            @foreach($clubs as $club)
                                <tr>
                                    <td class="px-3 py-2 whitespace-nowrap text-sm font-medium text-gray-600 dark:text-gray-400 text-left">
                                        {{$club->name}}
                                    </td>
                                    <td class="px-3 py-2 whitespace-nowrap text-sm text-gray-600 dark:text-gray-400 text-center">
                                        13
                                    </td>
                                    <td class="px-3 py-2 whitespace-nowrap text-sm text-gray-600 dark:text-gray-400 text-center">
                                        13
                                    </td>
                                    <td class="px-3 py-2 whitespace-nowrap text-sm text-gray-600 dark:text-gray-400 text-center">
                                        13
                                    </td>
                                    <td class="px-3 py-2 whitespace-nowrap text-sm text-gray-600 dark:text-gray-400 text-center">
                                        13
                                    </td>
                                    <td class="px-3 py-2 whitespace-nowrap text-sm text-gray-600 dark:text-gray-400 text-center">
                                        13
                                    </td>
                                    <td class="px-3 py-2 whitespace-nowrap text-sm text-gray-600 dark:text-gray-400 text-center">
                                        13
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="p-6 border-t border-gray-200 dark:border-gray-700 md:border-t-0 md:border-l">
                        <h2 class="px-6 py-3 text-left text-xs font-medium text-gray-900 dark:text-white uppercase tracking-wider text-center">Match Results</h2>
                        <table class="min-w-full divide-y divide-gray-200 dark:divide-gray-800 table-auto">
                            <thead class="bg-gray-50 dark:bg-gray-900">
                            <tr>
                                <th scope="col" colspan="3" class="px-6 py-3 text-left text-xs font-medium text-gray-900 dark:text-white uppercase tracking-wider text-center">
                                    <span class="ordinal">{{ $season->week }}th</span> Week Match Result
                                </th>
                            </tr>
                            </thead>
                            <tbody class="divide-y divide-gray-200 dark:divide-gray-800">
                            <tr>
                                <td class="px-6 py-2 whitespace-nowrap text-sm font-medium text-gray-600 dark:text-gray-400">
                                    Jane Cooper
                                </td>
                                <td class="px-6 py-2 whitespace-nowrap text-sm text-gray-600 dark:text-gray-400 text-right">
                                    3 - 2
                                </td>
                                <td class="px-6 py-2 whitespace-nowrap text-sm font-medium text-gray-600 dark:text-gray-400">
                                    Jane Cooper
                                </td>
                            </tr>


                            <!-- More people... -->
                            </tbody>
                        </table>
                    </div>

                    <div class="p-6 border-t border-gray-200 dark:border-gray-700 col-span-2">
                        <div class="flex justify-between">
                            <button type="button" class="inline-flex items-center px-3.5 py-2 border border-transparent text-sm leading-4 font-medium rounded-full shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                Play All
                                <svg xmlns="http://www.w3.org/2000/svg" class="ml-2 -mr-1 h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                                    <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zM9.555 7.168A1 1 0 008 8v4a1 1 0 001.555.832l3-2a1 1 0 000-1.664l-3-2z" clip-rule="evenodd" />
                                </svg>
                            </button>
                            <button type="button" class="inline-flex items-center px-3.5 py-2 border border-transparent text-sm leading-4 font-medium rounded-full shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                Next Week

                                <svg xmlns="http://www.w3.org/2000/svg" class="ml-2 -mr-1 h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                                    <path fill-rule="evenodd" d="M10.293 5.293a1 1 0 011.414 0l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414-1.414L12.586 11H5a1 1 0 110-2h7.586l-2.293-2.293a1 1 0 010-1.414z" clip-rule="evenodd" />
                                </svg>
                            </button>
                        </div>
                    </div>

                </div>
                <div class="flex grid bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg m-4 w-1/3">
                    <div class="p-6">
                        <table class="min-w-full divide-y divide-gray-200 dark:divide-gray-800">
                            <thead class="bg-gray-50 dark:bg-gray-900">
                            <tr>
                                <th scope="col" colspan="2" class="px-6 py-3 text-left text-xs font-medium text-gray-900 dark:text-white uppercase tracking-wider text-center">
                                    4th Week of predictions of championship
                                </th>
                            </tr>
                            </thead>
                            <tbody class="divide-y divide-gray-200 dark:divide-gray-800">
                            <tr>
                                <td class="px-6 py-2 whitespace-nowrap text-sm font-medium text-gray-600 dark:text-gray-400">
                                    Jane Cooper
                                </td>
                                <td class="px-6 py-2 whitespace-nowrap text-sm text-gray-600 dark:text-gray-400 text-right">
                                    %10
                                </td>
                            </tr>
                            <tr>
                                <td class="px-6 py-2 whitespace-nowrap text-sm font-medium text-gray-600 dark:text-gray-400">
                                    Jane Cooper
                                </td>
                                <td class="px-6 py-2 whitespace-nowrap text-sm text-gray-600 dark:text-gray-400 text-right">
                                    %10
                                </td>
                            </tr>
                            <tr>
                                <td class="px-6 py-2 whitespace-nowrap text-sm font-medium text-gray-600 dark:text-gray-400">
                                    Jane Cooper
                                </td>
                                <td class="px-6 py-2 whitespace-nowrap text-sm text-gray-600 dark:text-gray-400 text-right">
                                    %10
                                </td>
                            </tr>
                            <tr>
                                <td class="px-6 py-2 whitespace-nowrap text-sm font-medium text-gray-600 dark:text-gray-400">
                                    Jane Cooper
                                </td>
                                <td class="px-6 py-2 whitespace-nowrap text-sm text-gray-600 dark:text-gray-400 text-right">
                                    %10
                                </td>
                            </tr>

                            <!-- More people... -->
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @endforeach

    </div>
</div>
